@echo off
pushd e:\epg_external
if exist cvut.xml del cvut.xml
if exist wepg.xml del wepg.xml
wget http://televize.sh.cvut.cz/xmltv/all.xml
ren all.xml cvut.xml
wget http://wepg.cz/guide.xml
ren guide.xml wepg.xml
popd

rem update playlist
call e:\htpc\playlist\playlist.cmd