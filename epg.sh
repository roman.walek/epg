#!/bin/bash
timestampstart=$(date +"%F %T")

/opt/wg++/run.sh
cd /htpc/EPG
cp /opt/wg++/WebGrab++.log.txt epg.log
cp /opt/wg++/tv.sms.cz.m.channels.xml tv.sms.cz.m.channels.xml
cp epg-tmp.xml epg.xml

timestampstop=$(date +"%F %T")

git add *.*
git commit -m "$timestampstart --> $timestampstop"
./dropbox_uploader.sh upload ./epg.xml /EPG/epg.xml
./dropbox_uploader.sh upload ./WebGrab++.config.xml /EPG/WebGrab++.config.xml
git push -v --progress "origin" master:master 